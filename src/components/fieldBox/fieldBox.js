import React from 'react';
import Field from "../field/field";
import  './fieldBox.css'

const FieldBox = (props) => {

    return (
        <div className="fieldBox">
            {
                props.fields.map(f => (
                        <Field
                            index = {f.index}
                            key = {f.index}
                            hasItem = {f.hasItem}
                            open = {f.open}
                            openField = {() => props.openField(f.index, f.open, f.hasItem)}
                        >
                        </Field>
                    )
                )
            }
        </div>
    )
}

export default FieldBox;
