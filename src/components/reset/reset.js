import React from 'react';

const Reset = ({reset}) => {
    return (
        <button
        onClick={reset}
        >
            Reset game
        </button>
    );
};

export default Reset;