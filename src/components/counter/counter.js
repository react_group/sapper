import React from 'react';
import './counter.css'

const Counter = ({count, status}) => {
    return (
        <div className="counter">
            <p>you have already made {count} moves!</p>
            {!status? 'You won!!!' : ''}
        </div>
    );
};

export default Counter;