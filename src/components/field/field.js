import React from 'react';
import './field.css'

const Field = (props) => {

    const fieldClasses = ['field', ];

    if (props.hasItem) {
        fieldClasses.push('hasItem');
    }

    if (props.open) {
        fieldClasses.push('open');
    }

    return (
        <div className={fieldClasses.join(' ')}
             onClick={props.openField}
        >
        </div>
    );
};

export default Field;