import React from 'react';
import FieldBox from "./components/fieldBox/fieldBox";
import Counter from "./components/counter/counter";
import './App.css'
import Reset from "./components/reset/reset";

class App extends React.Component {

  constructor() {
    super();
    this.field = this.__createNewField();
    this.state = {
      field: this.field,
      game: true,
      count: 0
    }
  }


  __createNewField() {
    const allFields = [];
    const y = Math.floor(Math.random() * 36);
    for (let i =0; i < 36; i ++) {
      if (i === y) {
        let field = {
          index: i,
          hasItem: true,
          open: false
        }
        allFields.push(field);
      } else {
        let field = {
          index: i,
          hasItem: false,
          open: false
        }
        allFields.push(field)
      }
    }
    return allFields;
  }


  openField = (id, open, hasItem) => {
    if (this.state.game) {
      this.setState({
        count: this.state.count + 1
      })
      if (hasItem) {
        let fieldCopy = [];
        if (open === false) {
          fieldCopy = this.state.field.map(f => {
            if (f.index === id) {
              return {...f, open: true}
            }
            return f;
          })
          this.setState({
            field: fieldCopy
          })
        }

        this.setState({
          game: false
        })
      } else {
        let fieldCopy = [];
        if (open === false) {
          fieldCopy = this.state.field.map(f => {
            if (f.index === id) {
              return {...f, open: true}
            }
            return f;
          })

          this.setState({
            field: fieldCopy
          })
        }
      }
    }
  }

  reset = () => {
    const newState = {
      field: this.__createNewField(),
      game: true,
      count: 0
    }

    this.setState(newState);
  }




  render() {
    return (
        <div className="App container">
          <FieldBox
              fields = {this.state.field}
              openField = {(id, open, hasItem) => this.openField(id, open, hasItem)}
          >
          </FieldBox>

          <Counter
              count = {this.state.count}
              status = {this.state.game}
          />

          <Reset
              reset = {() => this.reset()}
          />
        </div>
    );
  }
}

export default App;